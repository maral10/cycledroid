/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.tripslist;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.maral.cycledroid.AppInfo;
import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.TextFormatter;
import com.maral.cycledroid.activity.file.ChooseFileActivity;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.asynctask.AsyncTaskQueue;
import com.maral.cycledroid.asynctask.AsyncTaskQueueImpl;
import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.DatabaseSQLite;
import com.maral.cycledroid.exporter.CSVExporter;
import com.maral.cycledroid.exporter.ExportTripTask;
import com.maral.cycledroid.exporter.Exporter;
import com.maral.cycledroid.exporter.GPXExporter;
import com.maral.cycledroid.exporter.KMLExporter;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.TripsList;

public final class MultiExportActivity extends MultiSelectActivity {
	private class Controller implements AsyncTaskReceiver {
		private boolean ourTask(ExtendedAsyncTask task) {
			for(long id : tasksIds)
				if(task.getId() == id)
					return true;
			return false;
		}
		
		public void updateProgress(ExtendedAsyncTask task, int progress) {
			if(ourTask(task))
				if(progressDialog != null)
					progressDialog.setProgress((100 * tripsDone + progress) / tripsCount);
		}
		
		public void taskStarts(ExtendedAsyncTask task) {
			if(ourTask(task)) {
				if(progressDialog == null) {
					progressDialog = new ProgressDialog(MultiExportActivity.this);
					progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
					progressDialog.setMax(100);
					progressDialog.setCancelable(false);
					progressDialog.setMessage(getString(R.string.progress_exporting_trips));
					progressDialog.show();
				}
				progressDialog.setSecondaryProgress(100 * (tripsDone + 1) / tripsCount);
			}
		}
		
		public void taskFinishes(ExtendedAsyncTask task) {
			if(ourTask(task)) {
				ExportTripTask exportTask = (ExportTripTask)task;
				Trip trip = exportTask.getTrip();
				String path = exportTask.getExportPath();
				CharSequence message;
				if(exportTask.wasExported()) {
					++tripsExported;
					message = TextFormatter.getText(MultiExportActivity.this, R.string.toast_trip_exported,
							trip.getName(), path);
				} else
					message = TextFormatter.getText(MultiExportActivity.this, R.string.toast_trip_not_exported,
							trip.getName(), path);
				Toast.makeText(MultiExportActivity.this, message, Toast.LENGTH_LONG).show();
				asyncTaskQueue.clearLast();
				if(++tripsDone == tripsCount) {
					removeProgressDialog();
					if(tripsExported == tripsDone)
						message = TextFormatter.getText(MultiExportActivity.this, R.string.toast_all_exported,
								exportPath);
					else
						message = TextFormatter.getText(MultiExportActivity.this, R.string.toast_not_all_exported,
								tripsExported, tripsCount, exportPath);
					Toast.makeText(MultiExportActivity.this, message, Toast.LENGTH_LONG).show();
					finish();
				} else {
					progressDialog.setSecondaryProgress(100 * (tripsDone + 1) / tripsCount);
					progressDialog.setProgress(100 * tripsDone / tripsCount);
				}
			}
		}
	}
	
	private static enum ExportType {
		CSV, GPX, KML,
	}
	
	private static enum SavedInstanceKey {
		TRIPS_COUNT, TRIPS_DONE, TRIPS_EXPORTED, EXPORT_PATH, TASKS_IDS,
	}
	
	private Controller controller = new Controller();
	
	private static final int DIALOG_FILE_FORMAT = 2;
	
	private AppInfo appInfo;
	private Database database;
	private ProgressDialog progressDialog = null;
	private AsyncTaskQueue asyncTaskQueue;
	
	private long[] tasksIds = new long[0];
	private int tripsCount = 0;
	private int tripsDone = 0;
	private int tripsExported = 0; // successfully
	private String exportPath = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			tripsCount = savedInstanceState.getInt(SavedInstanceKey.TRIPS_COUNT.name());
			tripsDone = savedInstanceState.getInt(SavedInstanceKey.TRIPS_DONE.name());
			tripsExported = savedInstanceState.getInt(SavedInstanceKey.TRIPS_EXPORTED.name());
			exportPath = savedInstanceState.getString(SavedInstanceKey.EXPORT_PATH.name());
		} catch(NullPointerException exception) {}
		try {
			getDoneButton().setText(R.string.export_data);
		} catch(NullPointerException exception) {}
		Settings settings = SettingsSystem.getInstance(this);
		appInfo = new AppInfo(this);
		database = DatabaseSQLite.getInstance(this, settings);
		try {
			tasksIds = savedInstanceState.getLongArray(SavedInstanceKey.TASKS_IDS.name());
		} catch(NullPointerException exception) {}
		asyncTaskQueue = AsyncTaskQueueImpl.getInstance();
		asyncTaskQueue.attach(controller);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(SavedInstanceKey.TRIPS_COUNT.name(), tripsCount);
		outState.putInt(SavedInstanceKey.TRIPS_DONE.name(), tripsDone);
		outState.putInt(SavedInstanceKey.TRIPS_EXPORTED.name(), tripsExported);
		outState.putString(SavedInstanceKey.EXPORT_PATH.name(), exportPath);
		outState.putLongArray(SavedInstanceKey.TASKS_IDS.name(), tasksIds);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		asyncTaskQueue.detach(controller);
		database.finish(isFinishing());
		removeProgressDialog();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_export_action, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.export:
				selectDone();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected Dialog onCreateDialog(final int id) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		DialogInterface.OnClickListener listener;
		
		switch(id) {
			case DIALOG_FILE_FORMAT:
				final String[] types = new String[ExportType.values().length];
				types[ExportType.CSV.ordinal()] = getString(R.string.csv);
				types[ExportType.GPX.ordinal()] = getString(R.string.gpx);
				types[ExportType.KML.ordinal()] = getString(R.string.kml);
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						export(ExportType.values()[item]);
						removeDialog(id);
					}
				};
				builder.setTitle(R.string.dialog_export_format);
				builder.setItems(types, listener);
				return builder.create();
		}
		
		return super.onCreateDialog(id);
	}
	
	@Override
	protected void processSelection(long[] ids) {
		showDialog(DIALOG_FILE_FORMAT);
	}
	
	private void export(ExportType type) {
		Intent intent = (new Intent(this, ChooseFileActivity.class));
		intent.putExtra(ChooseFileActivity.INTENT_MODE, ChooseFileActivity.MODE_SAVE_DIRECTORY);
		String startPath = appInfo.getLastExportPath();
		if(startPath == AppInfo.NO_IMPORT_PATH || !(new File(startPath).exists()))
			startPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		intent.putExtra(ChooseFileActivity.INTENT_START_PATH, startPath);
		startActivityForResult(intent, type.ordinal());
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == Activity.RESULT_OK) {
			ExportType type;
			try {
				type = ExportType.values()[requestCode];
			} catch(IndexOutOfBoundsException exception) {
				return;
			}
			
			exportPath = data.getStringExtra(ChooseFileActivity.RESULT_PATH);
			File file = new File(exportPath);
			if(!file.exists())
				file.mkdirs();
			appInfo.setLastExportPath(exportPath);
			Exporter exporter = null;
			String extension = null;
			switch(type) {
				case CSV:
					exporter = new CSVExporter(database);
					extension = ".csv";
					break;
				case GPX:
					exporter = new GPXExporter(this, database);
					extension = ".gpx";
					break;
				case KML:
					exporter = new KMLExporter(database);
					extension = ".kml";
					break;
			}
			
			TripsList list = database.getTripsList();
			List<String> names = new ArrayList<String>();
			long[] ids = getSelectedIds();
			tasksIds = new long[ids.length];
			tripsCount = ids.length;
			tripsDone = 0;
			for(int i = 0; i < ids.length; ++i) {
				Trip trip = list.getById(ids[i]);
				
				// handle duplicated names (add suffix)
				String realName = ChooseFileActivity.replaceIllegalCharacters(trip.getName());
				String currentName = realName;
				int currentSuffix = 1;
				boolean check;
				do {
					check = false;
					for(String n : names) {
						if(n.compareTo(currentName) == 0) {
							currentName = realName + " " + (++currentSuffix);
							check = true;
							break;
						}
					}
				} while(check);
				names.add(currentName);
				
				String filename = new File(exportPath + "/" + currentName + extension).getAbsolutePath();
				ExtendedAsyncTask task = new ExportTripTask(asyncTaskQueue, filename, exporter, trip);
				tasksIds[i] = task.getId();
				asyncTaskQueue.addTask(task);
			}
		}
	}
	
	private void removeProgressDialog() {
		if(progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
}
