/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.tripslist;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;

import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.DonateActivity;
import com.maral.cycledroid.activity.FormatterSettings;
import com.maral.cycledroid.activity.HelpActivity;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsActivity;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.DatabaseSQLite;
import com.maral.cycledroid.model.TripsList;
import com.maral.cycledroid.ui.ActivityManager;
import com.maral.cycledroid.ui.ActivityManagerSettings;

abstract class MultiSelectActivity extends ListActivity {
	private final class Controller implements OnClickListener, OnItemClickListener, Observer {
		public void onClick(View view) {
			switch(view.getId()) {
				case R.id.multi_select_done:
					selectDone();
					break;
				case R.id.select_all:
					selectAll();
					break;
			}
		}
		
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			updateSelectAllCheckBox();
		}
		
		public void update(Observable observable, Object data) {
			// updating always is not the most efficient solution
			updateTripsList();
			updateSelectAllCheckBox();
		}
	}
	
	private final Controller controller = new Controller();
	
	private static enum DialogType {
		NOTHING_CHOSEN,
	}
	
	private CheckBox selectAllCheckBox;
	
	private Settings settings;
	private Database database;
	private TripsList tripsList;
	private ActivityManager activityManager;
	
	protected abstract void processSelection(long[] ids);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		settings = SettingsSystem.getInstance(this);
		database = DatabaseSQLite.getInstance(this, settings);
		tripsList = database.getTripsList();
		activityManager = new ActivityManagerSettings(this, settings);
		
		setContentView(R.layout.multi_select);
		// button "done" is not shown in newer Android versions
		try {
			getDoneButton().setOnClickListener(controller);
		} catch(NullPointerException exception) {}
		selectAllCheckBox = (CheckBox)findViewById(R.id.select_all_checkbox);
		findViewById(R.id.select_all).setOnClickListener(controller);
		
		getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		getListView().setOnItemClickListener(controller);
		FormatterSettings formatter = new FormatterSettings(this, settings);
		setListAdapter(new TripsListAdapter(this, tripsList, formatter, true));
		
		tripsList.addObserver(controller);
		settings.addObserver(controller);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		settings.deleteObserver(controller);
		tripsList.deleteObserver(controller);
		activityManager.detachActivity(this);
		database.finish(isFinishing());
	}
	
	protected Button getDoneButton() {
		return (Button)findViewById(R.id.multi_select_done);
	}
	
	private void updateTripsList() {
		Runnable run = new Runnable() {
			public void run() {
				((BaseAdapter)getListAdapter()).notifyDataSetChanged();
			}
		};
		runOnUiThread(run);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_settings, menu);
		inflater.inflate(R.menu.option_help, menu);
		inflater.inflate(R.menu.option_donate, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.donate).setVisible(!settings.getRemoveDonate());
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			case R.id.help:
				(new HelpActivity.HelpOpener(this, getString(R.string.help_file))).openHelp();
				return true;
			case R.id.donate:
				startActivity(new Intent(this, DonateActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	private List<Integer> getCheckedPositionsList() {
		SparseBooleanArray checked = getListView().getCheckedItemPositions();
		List<Integer> result = new ArrayList<Integer>();
		for(int i = 0; i < getListView().getCount(); ++i)
			if(checked.get(i, false))
				result.add(i);
		return result;
	}
	
	protected void selectDone() {
		List<Integer> checked = getCheckedPositionsList();
		if(checked.size() == 0)
			showDialog(DialogType.NOTHING_CHOSEN.ordinal());
		else
			processSelection(getSelectedIds());
	}
	
	protected long[] getSelectedIds() {
		List<Integer> checked = getCheckedPositionsList();
		long[] ids = new long[checked.size()];
		for(int i = 0; i < checked.size(); ++i)
			ids[i] = tripsList.get(checked.get(i)).getId();
		return ids;
	}
	
	private void updateSelectAllCheckBox() {
		Runnable run = new Runnable() {
			public void run() {
				selectAllCheckBox.setChecked(allChecked());
			}
		};
		runOnUiThread(run);
	}
	
	private boolean allChecked() {
		return getCheckedPositionsList().size() == tripsList.size();
	}
	
	private void selectAll() {
		// if all elements are selected, we deselect all
		boolean select = !allChecked();
		for(int i = 0; i < getListView().getCount(); ++i)
			getListView().setItemChecked(i, select);
		updateSelectAllCheckBox();
	}
	
	@Override
	protected Dialog onCreateDialog(final int id) {
		DialogType dialogType;
		try {
			dialogType = DialogType.values()[id];
		} catch(IndexOutOfBoundsException exception) {
			return super.onCreateDialog(id);
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				removeDialog(id);
			}
		};
		
		switch(dialogType) {
			case NOTHING_CHOSEN:
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.error);
				builder.setMessage(R.string.alert_message_nothing_chosen);
				builder.setNeutralButton(R.string.ok, listener);
				return builder.create();
		}
		
		return super.onCreateDialog(id);
	}
}
