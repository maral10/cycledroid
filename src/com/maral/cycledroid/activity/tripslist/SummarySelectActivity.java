/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.tripslist;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.maral.cycledroid.AppInfo;
import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.trip.TripActivity;

public final class SummarySelectActivity extends MultiSelectActivity {
	private AppInfo appInfo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			getDoneButton().setText(R.string.show_summary);
		} catch(NullPointerException exception) {}
		appInfo = new AppInfo(this);
		if(savedInstanceState == null && !appInfo.getEnteredSummary())
			Toast.makeText(this, R.string.summary_first_hint, Toast.LENGTH_LONG).show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_show_summary, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.show_summary:
				selectDone();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void processSelection(long[] ids) {
		appInfo.setEnteredSummary(true);
		Intent intent = new Intent(this, TripActivity.class);
		intent.putExtra(TripActivity.INTENT_TRIPS_ARRAY, ids);
		startActivity(intent);
		finish();
	}
}
