/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.mock;

import android.content.Context;

import com.maral.cycledroid.R;

public class ExampleTripLakes extends ExampleTrip {
	public ExampleTripLakes(Context context) {
		super(context, R.string.example_lakes_name, R.string.empty_string);
	}
	
	@Override
	public Long getId() {
		return -3L;
	}
	
	@Override
	public float getDistance() {
		return 69111.84f;
	}
	
	@Override
	public float getTime() {
		return 13695000f;
	}
	
	@Override
	public Float getCurrentSpeed() {
		return 4.95f;
	}
	
	@Override
	public Float getMaxSpeed() {
		return 9.54f;
	}
	
	@Override
	public Float getAltitude() {
		return 107.25f;
	}
	
	@Override
	public float getElevationAsc() {
		return 115.80f;
	}
	
	@Override
	public float getElevationDesc() {
		return 134.24f;
	}
	
	@Override
	public Float getMinAltitude() {
		return 79.43f;
	}
	
	@Override
	public Float getMaxAltitude() {
		return 121.49f;
	}
	
	@Override
	public float getTotalTime() {
		return 19547000f;
	}
	
	@Override
	public Float getBearing() {
		return 129.40f;
	}
	
	@Override
	public Float getSlope() {
		return 4.23f;
	}
	
	@Override
	public Float getPowerFactor() {
		return 13.04f;
	}
	
	@Override
	public Float getInitialAltitude() {
		return 82.15f;
	}
	
	@Override
	public Long getStartTime() {
		return dateFromString("28.06.2013 10:22:06");
	}
	
	@Override
	public Long getEndTime() {
		return dateFromString("28.06.2013 17:15:53");
	}
}
