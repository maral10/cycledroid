/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.file;

import java.io.File;
import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.DonateActivity;
import com.maral.cycledroid.activity.HelpActivity;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsActivity;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.ui.ActivityManager;
import com.maral.cycledroid.ui.ActivityManagerSettings;

public final class ChooseFileActivity extends ListActivity {
	private static enum DialogType {
		CANT_READ, CONFIRM_OVERWRITE, CONFIRM_OVERWRITE_DIRECTORY, INCORRECT_FILENAME, CHOOSE_DIRECTORY,
	}
	
	private static enum SavedInstanceKey {
		CURRENT_PATH, FILENAME,
	}
	
	private static enum PreviousInstanceKey {
		HISTORY,
	}
	
	private static enum IntentKey {
		MODE, START_PATH, FILENAME_SUFFIX, FILENAME_PREFIX,
	}
	
	private class Controller implements OnClickListener, Comparator<String>, InputFilter {
		public void onClick(View view) {
			switch(view.getId()) {
				case R.id.cancel:
				case R.id.cancel_open:
					buttonCancel();
					break;
				case R.id.save:
					buttonSave();
					break;
				case R.id.open:
					buttonOpen();
					break;
				case R.id.select_all:
					adapter.checkAll(!adapter.allChecked());
					updateSelectAllCheckBox();
					break;
			}
		}
		
		public int compare(String lhs, String rhs) {
			// we assume that there is only one ROOT on the list
			if(lhs.compareTo(PATH_ROOT) == 0)
				return -1;
			if(rhs.compareTo(PATH_ROOT) == 0)
				return 1;
			// we assume, that there is only one PARENT on the list
			if(lhs.compareTo(PATH_PARENT) == 0)
				return -1;
			if(rhs.compareTo(PATH_PARENT) == 0)
				return 1;
			
			File file1 = new File(currentPath + "/" + lhs);
			File file2 = new File(currentPath + "/" + rhs);
			if(file1.isDirectory() && !file2.isDirectory())
				return -1;
			if(file2.isDirectory() && !file1.isDirectory())
				return 1;
			return collator.compare(file1.getName(), file2.getName());
		}
		
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			if(containsIllegalCharacters(source.subSequence(start, end).toString())) {
				long currentTime = System.currentTimeMillis();
				if(currentTime - illegalToastShown > ILLEGAL_TOAST_INTERVAL) {
					Toast toast = Toast.makeText(ChooseFileActivity.this, R.string.illegal_characters,
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					illegalToastShown = currentTime;
				}
				return replaceIllegalCharacters(source.subSequence(start, end).toString(), "");
			}
			return null;
		}
	}
	
	private final Controller controller = new Controller();
	
	// save or open, see modes below
	public static final String INTENT_MODE = IntentKey.MODE.name();
	// directory to be shown after open
	public static final String INTENT_START_PATH = IntentKey.START_PATH.name();
	// part of the filename before cursor
	public static final String INTENT_FILENAME_SUFFIX = IntentKey.FILENAME_SUFFIX.name();
	// part of the filename after cursor
	public static final String INTENT_FILENAME_PREFIX = IntentKey.FILENAME_PREFIX.name();
	
	private static enum Mode {
		SAVE_SINGLE, OPEN_SINGLE, SAVE_DIRECTORY, OPEN_MULTIPLE,
	}
	
	public static final int MODE_SAVE_SINGLE = Mode.SAVE_SINGLE.ordinal();
	public static final int MODE_OPEN_SINGLE = Mode.OPEN_SINGLE.ordinal();
	public static final int MODE_SAVE_DIRECTORY = Mode.SAVE_DIRECTORY.ordinal();
	public static final int MODE_OPEN_MULTIPLE = Mode.OPEN_MULTIPLE.ordinal();
	
	private static enum ResultType {
		PATH, PATHS,
	}
	
	// selected file (for getStringExtra(String))
	public static final String RESULT_PATH = ResultType.PATH.name();
	// if multiple files requested
	public static final String RESULT_PATHS = ResultType.PATHS.name();
	
	private static final String PATH_ROOT = FilesListAdapter.PATH_ROOT;
	private static final String PATH_PARENT = FilesListAdapter.PATH_PARENT;
	
	private static final char ILLEGAL_CHARACTERS[] = {
		'\0', '/'
	};
	private static final long ILLEGAL_TOAST_INTERVAL = 3000;
	
	private Settings settings;
	private ActivityManager activityManager;
	
	private Mode mode;
	private String currentFilename = null;
	private String currentPath = null;
	private Stack<String> history;
	
	// last time when illegal character toast was shown
	private long illegalToastShown = 0;
	
	private final Collator collator = Collator.getInstance();
	
	private ViewGroup selectAll;
	private CheckBox selectAllCheckBox;
	private FilesListAdapter adapter;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.file_dialog_main);
		setResult(RESULT_CANCELED, getIntent());
		
		settings = SettingsSystem.getInstance(this);
		activityManager = new ActivityManagerSettings(this, settings);
		
		mode = Mode.values()[getIntent().getIntExtra(INTENT_MODE, Mode.OPEN_SINGLE.ordinal())];
		switch(mode) {
			case SAVE_SINGLE:
			case SAVE_DIRECTORY:
				// on newer devices there are no such buttons (action bar)
				try {
					findViewById(R.id.cancel).setOnClickListener(controller);
					findViewById(R.id.save).setOnClickListener(controller);
				} catch(NullPointerException exception) {}
				String filenamePrefix = getIntent().getStringExtra(INTENT_FILENAME_PREFIX);
				String filenameSuffix = getIntent().getStringExtra(INTENT_FILENAME_SUFFIX);
				if(containsIllegalCharacters(filenamePrefix) || containsIllegalCharacters(filenameSuffix)) {
					filenamePrefix = replaceIllegalCharacters(filenamePrefix);
					filenameSuffix = replaceIllegalCharacters(filenameSuffix);
					Toast toast = Toast.makeText(this, R.string.illegal_replaced, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				
				EditText filenameEditText = (EditText)findViewById(R.id.filename_edit);
				filenameEditText.requestFocus();
				filenameEditText.append(filenamePrefix == null? "" : filenamePrefix);
				filenameEditText.append(filenameSuffix == null? "" : filenameSuffix);
				filenameEditText.setSelection(filenamePrefix == null? 0 : filenamePrefix.length());
				filenameEditText.setFilters(new InputFilter[] {
					controller,
				});
				if(mode == Mode.SAVE_DIRECTORY) {
					filenameEditText.setHint(getString(R.string.directory_name_hint));
					setTitle(R.string.label_directory_dialog_save);
				} else { // Mode.SAVE_SINGLE
					filenameEditText.setHint(getString(R.string.filename_hint));
					setTitle(R.string.label_file_dialog_save);
				}
				((LinearLayout)findViewById(R.id.save_file_bar)).setVisibility(View.VISIBLE);
				break;
			case OPEN_SINGLE:
				setTitle(R.string.label_file_dialog_open);
				break;
			case OPEN_MULTIPLE:
				// on newer devices there are no such buttons (action bar)
				try {
					findViewById(R.id.cancel_open).setOnClickListener(controller);
					findViewById(R.id.open).setOnClickListener(controller);
				} catch(NullPointerException exception) {}
				selectAll = (ViewGroup)findViewById(R.id.select_all);
				selectAll.setVisibility(View.VISIBLE);
				selectAll.setOnClickListener(controller);
				setTitle(R.string.label_file_dialog_open_multiple);
				getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
				((LinearLayout)findViewById(R.id.open_files_bar)).setVisibility(View.VISIBLE);
				selectAllCheckBox = (CheckBox)findViewById(R.id.select_all_checkbox);
				break;
		}
		
		String startPathSaved = null;
		if(savedInstanceState != null) {
			currentFilename = savedInstanceState.getString(SavedInstanceKey.FILENAME.name());
			startPathSaved = savedInstanceState.getString(SavedInstanceKey.CURRENT_PATH.name());
		}
		String startPathOption = getIntent().getStringExtra(INTENT_START_PATH);
		String startPath;
		if(startPathSaved != null)
			startPath = startPathSaved;
		else if(startPathOption != null)
			startPath = startPathOption;
		else
			startPath = PATH_ROOT;
		try {
			startPath = (new File(startPath)).getCanonicalPath();
		} catch(IOException exception) {}
		getDirectory(startPath, false);
		Map<PreviousInstanceKey, Object> previousInstance = (Map<PreviousInstanceKey, Object>)getLastNonConfigurationInstance();
		if(previousInstance == null)
			history = new Stack<String>();
		else
			history = (Stack<String>)previousInstance.get(PreviousInstanceKey.HISTORY);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(SavedInstanceKey.CURRENT_PATH.name(), currentPath);
		if(currentFilename != null)
			outState.putString(SavedInstanceKey.FILENAME.name(), currentFilename);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		activityManager.detachActivity(this);
	}
	
	@Override
	public Object onRetainNonConfigurationInstance() {
		Map<PreviousInstanceKey, Object> map = new HashMap<PreviousInstanceKey, Object>();
		map.put(PreviousInstanceKey.HISTORY, history);
		return map;
	}
	
	private static boolean containsIllegalCharacters(String filename) {
		if(filename == null)
			return false;
		for(char c : ILLEGAL_CHARACTERS)
			if(filename.contains(Character.toString(c)))
				return true;
		return false;
	}
	
	// global policy for replacing illegal characters in filenames
	public static String replaceIllegalCharacters(String filename) {
		return replaceIllegalCharacters(filename, "-");
	}
	
	private static String replaceIllegalCharacters(String filename, String with) {
		for(char c : ILLEGAL_CHARACTERS)
			filename = filename.replace(Character.toString(c), with);
		return filename;
	}
	
	private void returnResult() {
		Intent intent = getIntent();
		intent.putExtra(RESULT_PATH, currentPath + "/" + currentFilename);
		setResult(RESULT_OK, intent);
		finish();
	}
	
	private void getDirectory(String directory, boolean back) {
		if(!back && currentPath != null)
			history.push(currentPath);
		try {
			directory = (new File(directory)).getCanonicalPath().toString();
		} catch(IOException exception) {}
		currentPath = directory;
		List<String> filesList = new ArrayList<String>();
		File[] files = new File(directory).listFiles();
		if(files == null) { // if argument is not a directory
			currentPath = PATH_ROOT;
			directory = currentPath;
			files = new File(directory).listFiles();
		}
		
		((TextView)findViewById(R.id.path)).setText(getText(R.string.location) + ": " + currentPath);
		if(currentPath.compareTo(PATH_ROOT) != 0) {
			filesList.add(PATH_ROOT);
			filesList.add(PATH_PARENT);
		}
		for(File file : files)
			filesList.add(file.getName());
		Collections.sort(filesList, controller);
		
		adapter = new FilesListAdapter(this, getListView(), currentPath, filesList, mode == Mode.OPEN_MULTIPLE);
		setListAdapter(adapter);
		if(mode == Mode.OPEN_MULTIPLE) {
			boolean selectAllEnabled = adapter.getFilesCount() > 0;
			selectAll.setEnabled(selectAllEnabled);
			selectAllCheckBox.setEnabled(selectAllEnabled);
			selectAllCheckBox.setChecked(false);
		}
	}
	
	@Override
	protected Dialog onCreateDialog(final int id) {
		DialogType dialogType;
		try {
			dialogType = DialogType.values()[id];
		} catch(IndexOutOfBoundsException exception) {
			return super.onCreateDialog(id);
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				removeDialog(id);
			}
		};
		
		switch(dialogType) {
			case CANT_READ:
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.error);
				builder.setMessage(R.string.alert_message_cant_read);
				builder.setPositiveButton(R.string.ok, listener);
				return builder.create();
			case CONFIRM_OVERWRITE:
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(which == Dialog.BUTTON_POSITIVE)
							returnResult();
						removeDialog(id);
					}
				};
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(currentFilename);
				builder.setMessage(R.string.confirm_message_overwrite);
				builder.setPositiveButton(R.string.yes, listener);
				builder.setNegativeButton(R.string.no, listener);
				return builder.create();
			case CONFIRM_OVERWRITE_DIRECTORY:
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(which == Dialog.BUTTON_POSITIVE)
							returnResult();
						removeDialog(id);
					}
				};
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle((new File(currentPath + "/" + currentFilename)).getName());
				builder.setMessage(R.string.confirm_message_directory_overwrite);
				builder.setPositiveButton(R.string.yes, listener);
				builder.setNegativeButton(R.string.no, listener);
				return builder.create();
			case INCORRECT_FILENAME:
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(currentFilename);
				builder.setMessage(R.string.alert_message_incorrect_filename);
				builder.setNeutralButton(R.string.ok, listener);
				return builder.create();
			case CHOOSE_DIRECTORY:
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.error);
				builder.setMessage(R.string.alert_message_choose_directory);
				builder.setNeutralButton(R.string.ok, listener);
				return builder.create();
		}
		
		return super.onCreateDialog(id);
	}
	
	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id) {
		File file = adapter.getItem(position);
		if(file.isFile()) {
			switch(mode) {
				case SAVE_SINGLE:
					((EditText)findViewById(R.id.filename_edit)).setText(file.getName());
					break;
				case SAVE_DIRECTORY:
					Toast.makeText(this, R.string.toast_choose_directory, Toast.LENGTH_SHORT).show();
					break;
				case OPEN_SINGLE:
					currentFilename = file.getName();
					returnResult();
					break;
				case OPEN_MULTIPLE:
					updateSelectAllCheckBox();
					break;
			}
		} else {
			getListView().setItemChecked(position, false); // uncheck if not a file
			if(file.isDirectory()) {
				if(file.canRead())
					getDirectory(file.getAbsolutePath(), false);
				else
					showDialog(DialogType.CANT_READ.ordinal());
			}
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if((keyCode == KeyEvent.KEYCODE_BACK)) {
			if(!history.empty())
				getDirectory(history.pop(), true);
			else
				return super.onKeyDown(keyCode, event);
			return true;
		} else
			return super.onKeyDown(keyCode, event);
	}
	
	private void buttonSave() {
		currentFilename = ((EditText)findViewById(R.id.filename_edit)).getText().toString();
		if(currentFilename.length() > 0 || mode == Mode.SAVE_DIRECTORY) {
			File file = new File(currentPath + "/" + currentFilename);
			if(file.exists()) {
				switch(mode) {
					case SAVE_DIRECTORY:
						if(file.isDirectory()) {
							if(file.list().length == 0)
								returnResult();
							else
								showDialog(DialogType.CONFIRM_OVERWRITE_DIRECTORY.ordinal());
						} else
							showDialog(DialogType.CHOOSE_DIRECTORY.ordinal());
						break;
					case SAVE_SINGLE:
						if(file.isFile())
							showDialog(DialogType.CONFIRM_OVERWRITE.ordinal());
						else
							showDialog(DialogType.INCORRECT_FILENAME.ordinal());
						break;
					case OPEN_MULTIPLE: // this method handles save button
					case OPEN_SINGLE:
						break;
				}
			} else
				returnResult();
		}
	}
	
	private void updateSelectAllCheckBox() {
		selectAllCheckBox.setChecked(adapter.getFilesCount() > 0 && adapter.allChecked());
	}
	
	private void buttonOpen() {
		List<String> result = new ArrayList<String>();
		for(String filename : adapter.getChecked())
			result.add(currentPath + "/" + filename);
		Intent intent = getIntent();
		intent.putExtra(RESULT_PATHS, result.toArray(new String[result.size()]));
		setResult(RESULT_OK, intent);
		finish();
	}
	
	private void buttonCancel() {
		finish();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		switch(mode) {
			case SAVE_SINGLE:
			case SAVE_DIRECTORY:
				inflater.inflate(R.menu.option_save, menu);
				break;
			case OPEN_MULTIPLE:
			case OPEN_SINGLE:
				inflater.inflate(R.menu.option_open, menu);
				break;
		}
		inflater.inflate(R.menu.option_cancel, menu);
		inflater.inflate(R.menu.option_settings, menu);
		inflater.inflate(R.menu.option_help, menu);
		inflater.inflate(R.menu.option_donate, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.donate).setVisible(!settings.getRemoveDonate());
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.save:
				buttonSave();
				return true;
			case R.id.cancel:
				buttonCancel();
				return true;
			case R.id.open:
				buttonOpen();
				return true;
			case R.id.settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			case R.id.help:
				(new HelpActivity.HelpOpener(this, getString(R.string.help_file))).openHelp();
				return true;
			case R.id.donate:
				startActivity(new Intent(this, DonateActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
