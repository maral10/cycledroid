/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.service;

import java.util.Observable;
import java.util.Observer;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;

import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.FormatterSettings;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.activity.trip.TripActivity;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.DatabaseSQLite;
import com.maral.cycledroid.formatter.ValuePair;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.service.ServiceState.State;

public class TripService extends Service {
	private static enum IntentKey {
		TRIP_ID,
	}
	
	private class Controller implements LocationListener, GpsStatus.Listener, Observer {
		public void onLocationChanged(Location location) {
			if(trip == null)
				return;
			// there was probably a race and location was being provided after service being destroyed
			long currentTime = SystemClock.elapsedRealtime();
			lastRealFixTime = currentTime;
			if(location.hasAltitude() && location.hasSpeed() && ignoredPoints++ >= IGNORE_POINTS) {
				lastOursFixTime = currentTime;
				if(location.getSpeed() >= MIN_SPEED)
					database.addPoint(location, trip);
				else
					pauseTrip();
			}
		}
		
		public void onProviderDisabled(String provider) {
			pauseTrip();
			serviceState.state = State.GPS_DISABLED;
			serviceState.satellitesCount = 0;
			NOTIFIER.setChanged();
			NOTIFIER.notifyObservers(serviceState);
		}
		
		public void onProviderEnabled(String provider) {
			serviceState.state = State.NO_FIX;
			NOTIFIER.setChanged();
			NOTIFIER.notifyObservers(serviceState);
		}
		
		public void onStatusChanged(String provider, int status, Bundle extras) {}
		
		public void onGpsStatusChanged(final int event) {
			switch(event) {
				case GpsStatus.GPS_EVENT_STOPPED:
					onProviderDisabled(null);
					break;
				case GpsStatus.GPS_EVENT_STARTED:
					onProviderEnabled(null);
					break;
				case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
					lastGpsStatus = locationManager.getGpsStatus(lastGpsStatus);
					final Iterable<GpsSatellite> gpsSatellites = lastGpsStatus.getSatellites();
					int satellitesCount = 0;
					for(@SuppressWarnings("unused")
					final GpsSatellite satellite : gpsSatellites)
						++satellitesCount;
					serviceState.satellitesCount = satellitesCount;
					NOTIFIER.setChanged();
					NOTIFIER.notifyObservers(serviceState);
					break;
			}
		}
		
		public void update(Observable observable, Object data) {
			if(observable == trip || observable == formatter)
				updateNotification();
		}
	}
	
	private final Controller controller = new Controller();
	
	public static final String INTENT_TRIP_ID = IntentKey.TRIP_ID.name();
	private static final int NO_KEY = -1;
	
	private static final String GPS_LOCK = "gps lock";
	
	private static final long MIN_INTERVAL = 1000; // [millisecond]
	private static final long NO_FIX_TIME = 3000; // [millisecond]
	private static final float MIN_SPEED = 0.4f; // [meter/second]
	private static final long STATUS_UPDATE_INTERVAL = 1000; // [millisecond]
	private static final int IGNORE_POINTS = 5;
	
	private static final int NOTIFICATION_ID = 2323; // can't be 0!
	
	private static final ServiceState serviceState = new ServiceState(State.SERVICE_DISABLED, 0);
	public static final ServiceStatusNotifier NOTIFIER = new ServiceStatusNotifier();
	
	private static TripService instance = null;
	
	private final Handler handler = new Handler();
	
	private Database database;
	private Settings settings;
	private FormatterSettings formatter;
	private Trip trip = null;
	private LocationManager locationManager;
	
	private GpsStatus lastGpsStatus = null;
	private Long lastRealFixTime = null;
	private Long lastOursFixTime = null;
	private int ignoredPoints;
	
	private Notification notification;
	private PendingIntent pendingIntent;
	private WakeLock gpsLock;
	
	private final Runnable UPDATE_STATUS = new Runnable() {
		public void run() {
			try {
				if(serviceState.state == State.GPS_DISABLED || serviceState.state == State.SERVICE_DISABLED)
					return;
				
				ServiceState.State previousState = serviceState.state;
				
				long currentTime = SystemClock.elapsedRealtime();
				boolean hasRealFix = lastRealFixTime != null && currentTime - lastRealFixTime < NO_FIX_TIME;
				boolean hasOursFix = lastOursFixTime != null && currentTime - lastOursFixTime < NO_FIX_TIME
						&& ignoredPoints >= IGNORE_POINTS;
				if(!hasOursFix)
					pauseTrip();
				if(hasRealFix && hasOursFix) {
					serviceState.state = State.FIX;
					if(trip.isPaused())
						database.increaseTotalTime(trip, STATUS_UPDATE_INTERVAL);
				} else if(hasRealFix && !hasOursFix)
					serviceState.state = State.ACQUIRING;
				else if(!hasRealFix)
					serviceState.state = State.NO_FIX;
				
				if(serviceState.state != previousState) {
					NOTIFIER.setChanged();
					NOTIFIER.notifyObservers(serviceState);
				}
			} finally {
				if(serviceState.state != State.FIX && serviceState.state != State.ACQUIRING)
					ignoredPoints = 0;
				handler.postDelayed(this, STATUS_UPDATE_INTERVAL);
			}
		}
	};
	
	public static TripService getInstance() {
		return instance;
	}
	
	public static ServiceState getState() {
		return serviceState;
	}
	
	public Trip getTrip() {
		return trip;
	}
	
	@Override
	public void onCreate() {
		instance = this;
		settings = SettingsSystem.getInstance(this);
		database = DatabaseSQLite.getInstance(this, settings);
		formatter = new FormatterSettings(this, settings);
		formatter.addObserver(controller);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		trip = database.getTripsList().getById(intent.getLongExtra(INTENT_TRIP_ID, NO_KEY));
		
		ignoredPoints = 0;
		locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		locationManager.addGpsStatusListener(controller);
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setAltitudeRequired(true);
		criteria.setBearingRequired(false);
		criteria.setCostAllowed(false);
		criteria.setSpeedRequired(true);
		String provider = locationManager.getBestProvider(criteria, false);
		controller.onProviderEnabled(provider);
		/* onProviderEnabled must be called before requestLocationUpdates, since latter may cause onProviderDisabled to
		 * be called, if GPS is disabled. */
		locationManager.requestLocationUpdates(provider, MIN_INTERVAL, 0.0f, controller);
		trip.addObserver(controller);
		createNotification();
		PowerManager powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
		gpsLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, GPS_LOCK);
		gpsLock.acquire();
		handler.post(UPDATE_STATUS);
		
		return START_REDELIVER_INTENT;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		instance = null;
		locationManager.removeGpsStatusListener(controller);
		locationManager.removeUpdates(controller);
		handler.removeCallbacks(UPDATE_STATUS);
		trip.stop();
		formatter.deleteObserver(controller);
		trip.deleteObserver(controller);
		trip = null; // to avoid improper results of getTrip()
		gpsLock.release();
		stopForeground(true);
		serviceState.state = State.SERVICE_DISABLED;
		serviceState.satellitesCount = 0;
		NOTIFIER.setChanged();
		NOTIFIER.notifyObservers(serviceState);
		database.finish(true);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	private void createNotification() {
		int drawable = R.drawable.ic_stat_notify_tracking;
		long time = System.currentTimeMillis();
		notification = new Notification(drawable, null, time);
		notification.flags |= Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
		
		Intent intent = new Intent(this, TripActivity.class);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);
		// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
		intent.putExtra(TripActivity.INTENT_TRIP_ID, trip.getId()); // just in case
		pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
		
		updateNotification();
	}
	
	private void updateNotification() {
		String speed = format(formatter.getCurrentSpeed(trip));
		String distance = format(formatter.getDistance(trip));
		String time = format(formatter.getTime(trip));
		String appName = getString(R.string.app_label);
		String text = speed + "   " + distance + "   " + time;
		notification.setLatestEventInfo(getApplicationContext(), appName, text, pendingIntent);
		startForeground(NOTIFICATION_ID, notification);
	}
	
	private void pauseTrip() {
		if(!trip.isPaused())
			trip.pause();
	}
	
	private String format(ValuePair pair) {
		return pair.getValue() + (pair.getUnit() == null || pair.getUnit().equals("")? "" : " " + pair.getUnit());
	}
}
