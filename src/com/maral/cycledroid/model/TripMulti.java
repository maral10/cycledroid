/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.model;

import java.util.Observable;
import java.util.Observer;

import com.maral.cycledroid.WeakObserver;

import android.location.Location;

public class TripMulti extends Trip implements Observer {
	private final Iterable<Trip> trips;
	
	private final String name;
	private float distance;
	private float time;
	private Float maxSpeed;
	private float elevationAsc;
	private float elevationDesc;
	private Float minAltitude;
	private Float maxAltitude;
	private float totalTime;
	private Long startTime;
	private Long endTime;
	
	public TripMulti(String name, Iterable<Trip> trips) {
		this.name = name;
		this.trips = trips;
		Observer weakObserver = new WeakObserver(this);
		for(Trip trip : trips)
			trip.addObserver(weakObserver); // values are being updated when trip changes
		calculateValues();
	}
	
	private void calculateValues() {
		distance = 0.0f;
		time = 0.0f;
		maxSpeed = null;
		elevationAsc = 0.0f;
		elevationDesc = 0.0f;
		minAltitude = null;
		maxAltitude = null;
		totalTime = 0.0f;
		startTime = null;
		endTime = null;
		
		for(Trip trip : trips) {
			distance += trip.getDistance();
			time += trip.getTime();
			maxSpeed = maxWithNulls(maxSpeed, trip.getMaxSpeed());
			elevationAsc += trip.getElevationAsc();
			elevationDesc += trip.getElevationDesc();
			minAltitude = minWithNulls(minAltitude, trip.getMinAltitude());
			maxAltitude = maxWithNulls(maxAltitude, trip.getMaxAltitude());
			totalTime += trip.getTotalTime();
			startTime = minWithNulls(startTime, trip.getStartTime());
			endTime = maxWithNulls(endTime, trip.getEndTime());
		}
	}
	
	private static Float minWithNulls(Float f1, Float f2) {
		if(f1 == null)
			return f2;
		if(f2 == null)
			return f1;
		return Math.min(f1, f2);
	}
	
	private static Float maxWithNulls(Float f1, Float f2) {
		if(f1 == null)
			return f2;
		if(f2 == null)
			return f1;
		return Math.max(f1, f2);
	}
	
	private static Long maxWithNulls(Long l1, Long l2) {
		if(l1 == null)
			return l2;
		if(l2 == null)
			return l1;
		return Math.max(l1, l2);
	}
	
	private static Long minWithNulls(Long l1, Long l2) {
		if(l1 == null)
			return l2;
		if(l2 == null)
			return l1;
		return Math.min(l1, l2);
	}
	
	@Override
	public Long getId() {
		return null;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String getDescription() {
		return null;
	}
	
	@Override
	public boolean providesEdit() {
		return false;
	}
	
	@Override
	public void edit(String name, String description) {
		throw new UnsupportedOperationException("This is a set of trips - it cannot be edited.");
	}
	
	@Override
	public boolean providesTracking() {
		return false;
	}
	
	private void throwTrackingException() {
		throw new UnsupportedOperationException("This is a set of trips - it does not provide tracking.");
	}
	
	@Override
	public void addPoint(Location point) {
		throwTrackingException();
	}
	
	@Override
	public void increaseTotalTime(float deltaTime) {
		throwTrackingException();
	}
	
	@Override
	public void pause() {
		throwTrackingException();
	}
	
	@Override
	public boolean isPaused() {
		throwTrackingException();
		return false; // whatever
	}
	
	@Override
	public void stop() {
		throwTrackingException();
	}
	
	@Override
	public float getDistance() {
		return distance;
	}
	
	@Override
	public float getTime() {
		return time;
	}
	
	@Override
	public Float getCurrentSpeed() {
		return null;
	}
	
	@Override
	public Float getMaxSpeed() {
		return maxSpeed;
	}
	
	@Override
	public Float getAltitude() {
		return null;
	}
	
	@Override
	public float getElevationAsc() {
		return elevationAsc;
	}
	
	@Override
	public float getElevationDesc() {
		return elevationDesc;
	}
	
	@Override
	public Float getMinAltitude() {
		return minAltitude;
	}
	
	@Override
	public Float getMaxAltitude() {
		return maxAltitude;
	}
	
	@Override
	public float getTotalTime() {
		return totalTime;
	}
	
	@Override
	public Float getBearing() {
		return null;
	}
	
	@Override
	public Float getSlope() {
		return null;
	}
	
	@Override
	public Float getPowerFactor() {
		return null;
	}
	
	@Override
	public Float getInitialAltitude() {
		return null;
	}
	
	@Override
	public Float getFinalAltitude() {
		return null;
	}
	
	@Override
	public Long getStartTime() {
		return startTime;
	}
	
	@Override
	public Long getEndTime() {
		return endTime;
	}
	
	@Override
	public boolean providesGraphs() {
		return false;
	}
	
	@Override
	public boolean providesMap() {
		return false;
	}
	
	@Override
	public boolean providesExport() {
		return false;
	}
	
	@Override
	public boolean providesShare() {
		return false;
	}
	
	public void update(Observable observable, Object data) {
		calculateValues();
		setChanged();
		notifyObservers();
	}
}
