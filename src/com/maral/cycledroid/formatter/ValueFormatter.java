/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.formatter;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.maral.cycledroid.model.Unit.Altitude;
import com.maral.cycledroid.model.Unit.Angle;
import com.maral.cycledroid.model.Unit.Distance;
import com.maral.cycledroid.model.Unit.Duration;
import com.maral.cycledroid.model.Unit.Energy;
import com.maral.cycledroid.model.Unit.Pace;
import com.maral.cycledroid.model.Unit.Power;
import com.maral.cycledroid.model.Unit.Speed;
import com.maral.cycledroid.model.Unit.Volume;
import com.maral.cycledroid.model.Unit.Weight;

public class ValueFormatter {
	private final NumberFormat FORMAT_ZERO = new FixedMinusZeroFormat("0");
	private final NumberFormat FORMAT_ONE = new FixedMinusZeroFormat("0.0");
	private final NumberFormat FORMAT_TWO = new FixedMinusZeroFormat("0.00");
	private final NumberFormat FORMAT_THREE = new FixedMinusZeroFormat("0.000");
	private final DateFormat FORMAT_DATE = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT);
	private final DateFormat FORMAT_TIME = SimpleDateFormat.getTimeInstance(SimpleDateFormat.MEDIUM);
	
	public String formatAltitude(Float value, Altitude unit) {
		if(value == null)
			return null;
		switch(unit) {
			case M:
			case FT:
				return FORMAT_ZERO.format(value);
			case KM:
			case MI:
			case NMI:
				return FORMAT_THREE.format(value);
		}
		throw new IllegalArgumentException("Formatting values of altitude unit " + unit + " is not supported.");
	}
	
	public String formatAngle(Float value, Angle unit) {
		if(value == null)
			return null;
		switch(unit) {
			case DEGREE:
			case PERCENT:
				return FORMAT_ZERO.format(value);
		}
		throw new IllegalArgumentException("Formatting values of angle unit " + unit + " is not supported.");
	}
	
	public String formatDate(Long date) {
		if(date == null)
			return null;
		return FORMAT_DATE.format(date) + "\n" + FORMAT_TIME.format(date);
	}
	
	public String formatDistance(Float value, Distance unit) {
		if(value == null)
			return null;
		switch(unit) {
			case M:
			case FT:
				return FORMAT_ZERO.format(value);
			case KM:
			case MI:
			case NMI:
				return FORMAT_TWO.format(value);
		}
		throw new IllegalArgumentException("Formatting values of distance unit " + unit + " is not supported.");
	}
	
	public String formatDuration(Float value, Duration unit) {
		if(value == null)
			return null;
		long longValue = Math.round(value);
		long seconds;
		switch(unit) {
			case MS:
				seconds = longValue / 1000;
				break;
			case S:
				seconds = longValue;
				break;
			default:
				throw new IllegalArgumentException("Formatting values of duration unit " + unit + " is not supported.");
		}
		
		long hours = seconds / 3600;
		seconds %= 3600;
		long minutes = seconds / 60;
		seconds %= 60;
		switch(unit) {
			case MS:
				return String.format(Locale.ENGLISH, "%02d:%02d:%02d.%03d", hours, minutes, seconds, longValue % 1000);
			case S:
				return String.format(Locale.ENGLISH, "%02d:%02d:%02d", hours, minutes, seconds);
		}
		throw new IllegalStateException("This should never happen.");
	}
	
	public String formatEnergy(Float value, Energy unit) {
		if(value == null)
			return null;
		switch(unit) {
			case KCAL:
				return FORMAT_ZERO.format(value);
		}
		throw new IllegalArgumentException("Formatting values of energy unit " + unit + " is not supported.");
	}
	
	public String formatPace(Float value, Pace unit) {
		if(value == null)
			return null;
		switch(unit) {
			case S_PER_KM:
			case S_PER_MI:
			case S_PER_NMI:
				return formatDuration(value, Duration.S);
			case MS_PER_FT:
			case MS_PER_M:
				return FORMAT_TWO.format(value);
		}
		throw new IllegalArgumentException("Formatting values of pace unit " + unit + " is not supported.");
	}
	
	public String formatPower(Float value, Power unit) {
		if(value == null)
			return null;
		switch(unit) {
			case W:
				return FORMAT_ZERO.format(value);
			case HP:
				return FORMAT_TWO.format(value);
		}
		throw new IllegalArgumentException("Formatting values of power unit " + unit + " is not supported.");
	}
	
	public String formatSpeed(Float value, Speed unit) {
		if(value == null)
			return null;
		switch(unit) {
			case FTS:
			case KMH:
			case KN:
			case MPH:
			case MS:
				return FORMAT_ONE.format(value);
		}
		throw new IllegalArgumentException("Formatting values of speed unit " + unit + " is not supported.");
	}
	
	public String formatVolume(Float value, Volume unit) {
		if(value == null)
			return null;
		switch(unit) {
			case GAL:
			case L:
				return FORMAT_ONE.format(value);
		}
		throw new IllegalArgumentException("Formatting values of volume unit " + unit + " is not supported.");
	}
	
	public String formatWeight(Float value, Weight unit) {
		if(value == null)
			return null;
		switch(unit) {
			case G:
				return FORMAT_ZERO.format(value);
			case OZ:
				return FORMAT_ONE.format(value);
		}
		throw new IllegalArgumentException("Formatting values of weight unit " + unit + " is not supported.");
	}
}
