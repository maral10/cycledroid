CycleDroid – computador de bike

CycleDroid permite que você mapeie seu percurso usando o GPS e lhe informa sobre: velocidade, distância, tempo, altitude, elevação, declive, calorias queimadas, gordura e muito mais. Uma rota pode ser exibida no mapa e publicada no Facebook. A aplicação permite criar percursos e associar dados coletados com um percurso específico. Também há a possibilidade de ver um índice dos percursos selecionados: distância total, tempo, etc.

Todos os dados coletados pelo aplicativo podem ser exportados para um local no formato CSV e então, importado para outro dispositivo com o CycleDroid. Também é possível salvar dados para um GPX (GPS eXchange Format) ou um arquivo KML e verificar a rota em um mapa no computador (para a instância do Google Earth).

CycleDroid também permite gerar gráficos com: altitude/distância, velocidade/distância, velocidade/tempo. Você pode facilmente dar zoom no gráfico usando o multi-touch. O aplicativo também permite salvar um gráfico em particular ou como parte de um arquivo de imagem.

Permissões:
– localização precisa (GPS e com base na rede) – para rastreamento,
– alterar ou excluir conteúdo de armazenamento USB, testar o acesso ao armazenamento protegido – para importar e exportar percursos,
– impedir modo de inatividade do aparelho – para manter a navegação habilitada quando o dispositivo não estiver sendo utilizado,
– acesso total à rede, ver conexões de rede, ler a configuração de serviço do Google – para o Google Maps e Facebook.

Sinta-se à vontade para classificar, informar falhas no aplicativo ou traduções, deixar suas opiniões nos comentários e me contatar, diretamente ou pelo perfil do CycleDroid no Facebook: http://www.facebook.com/cycledroid. Caso tenha gostado deste aplicativo, classifique-o no site. Caso queira me ajudar e participar da tradução do CycleDroid, entre neste site: http://crowdin.net/project/cycledroid.

===================================================

CycleDroid (donate)

This version of the application will no longer be updated. There is a free version available in the Play Store which is identical – please download that one instead. If you would like to donate my work, you can still do this using the in-app purchases in the free version.

===================================================

Donate CycleDroid

If you like this application, you can donate my work.

===================================================


– versão turco,
– versão espanhol – graças a Francisco M. Román para a tradução,
– versão bósnio – graças a Ishak Vehab para a tradução,
– smoother graphs,
– bug fixes,
– pequenas melhorias nas traduções.
